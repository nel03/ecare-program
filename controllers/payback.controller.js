const Payback = require('../models/payback.models')

// Record Payback of organization
async function addPayback(req, res){
    try{
        let newPayback = new Payback({
            date: req.body.date,
            amount: req.body.amount,
            capital: req.body.capital,
            ecare: req.body.ecare,
            church: req.body.church,
            organization: req.body.organization
        })
        const result = await newPayback.save();
        if(result){
            return res.send(true)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
};

// Project officer or the RDO update/Edit the Payback
async function updatePayback(req, res){
    const updatePayback = {
        capital: req.body.capital,
        ecare: req.body.ecare,
        church: req.body.church,
        organization: req.body.organization
    }
    try{
        const result = await Payback.findByIdAndUpdate(req.params.id, updatePayback)
        if(result){
           return res.send(updatePayback)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
};

// Check if the Payback added is correct
async function getPayback(req, res){
    try{
        const payback = await Payback.findById(req.params.id)
        if(payback){
            return res.send(Payback)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
};

// check all the Paybacks that was recorded
async function getAllPaybacks(req, res){
    try{
        const payback = await Payback.find({})
        if(payback){
            return res.send(Payback)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
};

module.exports = {
    addPayback: addPayback,
    updatePayback: updatePayback,
    getPayback: getPayback,
    getAllPayback: getAllPaybacks
}