const Release = require('../models/r2g.models');

//Record Release of R2G fund
async function addRelease(req, res){
    try{
        let newRelease = new Release ({
            date: req.body.date,
            r2gRelease: req.body.r2gRelease
        })
        const result = await newRelease.save()
        if(result){
            return res.send(true)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
}

//Update or Edit Release of R2G fund
async function updateRelease(req,res){
    const updateRelease = {
        date: req.body.date,
        r2gRelease: req.body.r2gRelease
    }
    try{
        const result = await Release.findByIdAndUpdate(req.params.id, updateRelease)
        if(result){
            return res.send(updateRelease)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
}

//Check the Release 
async function getRelease(req, res){
    try{
        const release = await Release.findById(req.params.id)
        if(release){
            return res.send(release)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
}

async function getAllReleases(req, res){
    try{
        const allReleases = await Release.find({})
        if(allReleases){
            return res.send(allReleases)
        }
        return res.send(false)
    }catch{
        console.log(error)
        return res.send(error)
    }
}


module.exports = {
    addRelease: addRelease,
    updateRelease: updateRelease,
    getRelease: getRelease,
    getAllReleases: getAllReleases
}