const express = require('express');
const r2gController = require('../controllers/r2g.controller');
const router =express.Router();

router.post('/release', r2gController.addRelease); // R2G releases is made to the organization

router.put('/:r2gCapitalId', r2gController.updateRelease) // Update or Edit R2G release to Org.

router.get('/release', r2gController.getRelease) // Check a specific release

router.get('/allRelease', r2gController.getAllReleases) // check all releases

module.exports = router