const express = require('express');
const paybackController = require('../controllers/payback.controller')
const router = express.Router();

router.post('/payback', paybackController.addPayback)//Record Payback of R2G

router.put('/:paybackId', paybackController.updatePayback)//Update Payback

router.get('/payback', paybackController.getPayback)//Check Payback

router.get('/allPayback', paybackController.getAllPayback)//Check All Payback

module.exports = router