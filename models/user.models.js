const mongoose = require('mongoose');

const userSchema = {
    name:{
        type: String,
        required: [true, 'Name is required']
    },
    isAdmin:{
        type: Boolean,
        default: false
    },
    mobileNo:{
        type: String,
        default: [true, 'Mobile number is required']
    },
    assignedOrganization:[
        {
            organizationId:{
                type: String,
                default: [true, 'Organization ID is required']
            },
            registeredOn:{
                type: Date,
                default: new Date()
            }
        }
    ]
}

module.exports = mongoose.model('Officer', userSchema);