const mongoose = required('mongoose')

const dioceseSchema = {
    dioceseCode: {
        type: String,
        required: [true, 'Abbreviation name is required']
    },
    name: {
        type: String,
        required: [true, 'Diocese name is required']
    },
    address:[
        {
            buildingName: {
                type: String,
                required: [true, 'Building name is required']
            },
            street:{
                type: String,
                required: [true, 'Street is required']
            },
            city:{
                type: String,
                required: [true, 'City is required']
            }
        }
    ],
    assignedUser:[
        {
            userId:{
                type: String,
                required: [true, 'User ID is required']
            },
            registeredOn:{
                type: Date,
                default: new Date()
            }
        }
    ]
}

module.exports = mongoose.model('Diocese', dioceseSchema)