const mongoose = require('mongoose');

const paybackSchema = {
    date: {
        type: Date,
        default: new Date()
    },
    amount:{
        type: Number,
        required: [true, 'Amount paid is required']
    },
    breakdownOfPayment:[
        {
            capital: Number,
            ecare: Number,
            church: Number,
            organization: Number
        }
    ]
}

module.exports = mongoose.model('Payback', paybackSchema);