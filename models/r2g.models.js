const mongoose = required('mongoose');

const r2gSchema = {
    date:{
        type: Date,
        default: new Date()
    },
    r2gRelease:{
        type: Number,
        required: [true, 'Receiver To Giver amount applied is required']
    },
    payback:[
        {
            paymentId:{
                type: String,
                required: [true, 'Payment Id is required']
            },
            registeredOn:{
                type: Date,
                default: new Date()
            }
        }
    ]
}

module.exports = mongoose.model('R2G', r2gSchema);