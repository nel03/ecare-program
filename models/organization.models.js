const mongoose = require('mongoose');

const organizationSchema = {
    name: {
        type: String,
        default: [true, 'Organization name is required']
    },
    address:{
        type: String,
        default: [true, 'Address is required']
    },
    appliedR2G:[
        {
            r2gId:{
                type: String,
                default: [true, 'Receiver To Giver ID is required']
            },
            registeredOn:{
                type: Date,
                default: new Date()
            }
        }
    ]
}

module.exports = mongoose.model('Organization', organizationSchema);